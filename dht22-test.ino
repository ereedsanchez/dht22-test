#include <ESP8266WiFi.h>
#include <ESP8266HTTPClient.h>
#include <DHTesp.h>
DHTesp dht;
void setupDHT() {
   dht.setup(0  , DHTesp::DHT11); // CONNECT TO D3 PIN ON ESP
  }
const char* ssid = "MyAlt";                   //Nombre de la RED
const char* password = "2805indigo";           //Password de la RED
WiFiClient espClient;
long lastMsg = 0;
char msg[100];
int value = 0;
int sensorPin = A0;
  void setup_wifi() {
  delay(10);
  // We start by connecting to a WiFi network
  Serial.println();
  Serial.print("Connecting to ");
  Serial.println(ssid);
  WiFi.begin(ssid, password);
  while (WiFi.status() != WL_CONNECTED) {
    delay(2000);
    Serial.print(".");
  }
  randomSeed(micros());
  Serial.println("");
  Serial.println("WiFi connected");
  Serial.println("IP address: ");
  Serial.println(WiFi.localIP());
}
  

void setup() {
  pinMode(BUILTIN_LED, OUTPUT);     // Initialize the BUILTIN_LED pin as an output
  Serial.begin(9600);
  setup_wifi();  
}





void postRequest(String msg)
{
   HTTPClient http;
  
   if (http.begin(espClient, "http://165.22.191.125:5002/v1/collector/")) //Iniciar conexión
   {
      Serial.print("[HTTP] POST...\n");
      http.addHeader("Content-Type", "application/json");
      int httpCode = http.POST(msg);  // Realizar petición
 
      if (httpCode > 0) {
         Serial.printf("[HTTP] POST... code: %d\n", httpCode);
 
         if (httpCode == HTTP_CODE_OK || httpCode == HTTP_CODE_MOVED_PERMANENTLY) {
            String payload = http.getString();   // Obtener respuesta
            Serial.println(payload);
         }
      }
      else {
         Serial.printf("[HTTP] GET... failed, error: %s\n", http.errorToString(httpCode).c_str());
      }
 
      http.end();
   }
   else {
      Serial.printf("[HTTP} Unable to connect\n");
   }
 
}



void loop() {
  long now = millis();
  if (now%3000 == 0) {
    String humidity = String(dht.getHumidity());
    String temperature = String(dht.getTemperature());
    Serial.println("sending message");
    snprintf(msg, 100, "{\"AppKey\":\"esp01\",\"NetKey\":\"esp01\",\"DeviceId\":\"CritterCam1\",\"Value\":\"%s\"}", humidity);
    Serial.println(msg);
    postRequest(msg);
    snprintf(msg, 100, "{\"AppKey\":\"esp01\",\"NetKey\":\"esp01\",\"DeviceId\":\"CritterCam1\",\"Value\":\"%s\"}", temperature);
    Serial.println(msg);
    postRequest(msg);
    
  }
}
